#' Set number of workers
#'
#' @param n_workers integer: defaults to \code{4}, set to 1 to have single threaded use
#'
#' @export
#'
#' @import furrr
#' @import future
#'
#' @examples setWorkers(10)
setWorkers <- function(n_workers = 4){
  options(future.rng.onMisuse = "ignore")
  fstDBenv[["n_workers"]] = n_workers
  plan(multisession, workers = n_workers)
}


#' Set location of DB
#'
#' @param loc character: Path to setup a database folder. All tables and metadat will be stored here
#'
#' @export
#'
#' @examples set.loc("~/saildb")
set.loc <- function(loc){
  if(!dir.exists(loc)){
    cat("Creating folder",loc,"\n")
    dir.create(loc)
  }
  fstDBenv[["loc"]] <- loc
}



#' Get a value from the package environment
#'
#' @param var character: variable to fetch
#'
#' @return value of variable in the env
#' @export
#'
#' @examples getVar("loc")
getVar <- function(var){
  if(is.null(fstDBenv[[var]])){
    stop(var, "not given and not set")
  }else{
    return(fstDBenv[[var]])
  }
}
