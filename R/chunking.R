#' Internal function getLengths
#'
#'
#' @param var vector: Variable to get the start & end positions of
#'
#' @return data.table with each lever of the variable and start and end positions
getLengths <- function(var){
  lens = rle(var)$lengths
  data.table(var = var
             ,rstart = rep( cumsum(c(1,lens[-length(lens)] )), lens)
             ,rend = rep(cumsum(lens), lens) )
}

#' Internal function getChunkDef
#'
#' @param dt data.table: data to get chunk definitions for
#' @param index_var character vector: Names of columns to use for indexing. Note that they should be in priority of indexing.
#' @param n_chunks integer: Number of chunks to output
#' @param len_chunks integer: Number of rows to allow in each chunk
#' @param index_chunks integer: Index of index_vars to use for chunk separation. This index var will automatically get top priority.
#'
#' @return data.table with chunk start and ends
getChunkDef <- function(dt, index_var, n_chunks, len_chunks, index_chunks) {
  if(sum(!missing(n_chunks),!missing(len_chunks),!missing(index_chunks))>1) stop("Can only use one chunking method at a time.")

  if(all(missing(n_chunks),missing(len_chunks),missing(index_chunks))){
    cat("Using default chunking:",getVar("n_worker"),"chunks\n")
    n_chunks = getVar("n_workers")
  }

  if(!missing(n_chunks)){
    ## Define Chunks
    chnks = dt %>% nrow() %>% seq(1,.,by=floor(./n_chunks)) %>% c(.,dt %>% nrow()) %>% unique()
    chnks = cbind(rstart = chnks[-length(chnks)]+1, rend = chnks[-1], nchnk = 1:(length(chnks)-1) ) %>% as.data.table()
    chnks[1,1] = chnks[1,1]-1
  }else if(!missing(len_chunks)){
    chnks = dt %>% nrow() %>% seq(1,.,by=floor(len_chunks)) %>% c(.,dt %>% nrow()) %>% unique()
    chnks = cbind(rstart = chnks[-length(chnks)]+1, rend = chnks[-1], nchnk = 1:(length(chnks)-1) ) %>% as.data.table()
    chnks[1,1] = chnks[1,1]-1
  }else{
    chnks = dt[,get(index_var[index_chunks])] %>% getLengths %>% unique
    chnks[,c("index_var", "nchnk") := .(NULL, seq_len(nrow(chnks)))]
  }
  return(chnks)
}

#' Internal function intersectIndex
#'
#' Get intersection of indices. Used for loading with multiple indices
#'
#' @param chunk_id integer: chunk id
#' @param var.x vector: LHS variable
#' @param var.y vector: RHS variable
#' @param rstart.x vector: LHS start index
#' @param rend.x vector: LHS end index
#' @param rstart.y vector: RHS start index
#' @param rend.y vector: RHS end index
#'
#' @return data.table of overlaping starts and ends
intersectIndex <- function(chunk_id,var.x,var.y,rstart.x,rend.x,rstart.y,rend.y){

  ret = data.table(chunk_id
                   ,var = paste0(var.x,"-",var.y)
                   ,rstart = pmax(rstart.x, rstart.y)
                   ,rend = pmin(rend.x, rend.y) )

  ret[rstart>rend,":="(rstart=NA
                       ,rend=NA)]
  return(ret)
}
