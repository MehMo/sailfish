#' Ingest a Static Data Source
#'
#' This function will ingest a data and index it inside a DB set by \code{loc} variable. This data is meant to be static (i.e. read-only).
#' Multiple variables can be used to index the table making lookups very fast using  \link[sailfish]{readStaticData}
#' 3 chunking schemes are available: 'n_chunks','len_chunks' and 'index_chunks'. Only one can be used at a time
#'
#' @param dt data.frame or data.table: Table to be ingested in the DB
#' @param ref_name character: Name of the table to be set in the DB
#' @param index_vars character vector: Names of columns to use for indexing. Note that they should be in priority of indexing.
#' @param n_chunks integer: Number of chunks to output
#' @param len_chunks integer: Number of rows to allow in each chunk
#' @param index_chunks integer: Index of index_vars to use for chunk separation. This index var will automatically get top priority.
#' @param loc character: path of the DB. Can be set globally with \link[sailfish]{set.loc}
#' @param overwrite logical: whether to overwrite if DB exists. Defaults \code{FALSE}
#' @param silent logical: whether to return the meta data or not return anything. Defaults \code{TRUE}
#'
#' @return metadata of written table if \code{silent=F}
#' @export
#'
#' @import data.table
#' @import fst
#' @import magrittr
#' @import purrr
#'
#' @examples
#' data = nycflights13::flights
#' ingestStaticData(data
#'                 , ref_name = "flights"
#'                 , index_vars = c("carrier","origin")
#'                 , n_chunks = 10
#'                 , loc = "~/saildb")
ingestStaticData <- function(dt, ref_name, index_vars, n_chunks, len_chunks, index_chunks, loc, overwrite = F, silent = T){

  if(missing(loc)) loc = getVar("loc")
  if(!missing(loc) & !dir.exists(loc)) dir.create(loc)

  if(missing(index_vars))    stop("index_var must be given")

  if(missing(ref_name)){
    stop("ref_name must be given")
  }else if(ref_name %in% getTblList(loc)){
    if(overwrite) unlink(file.path(loc,ref_name), recursive = T)
      else stop("ref_name not unique in loc")
  }

  setDT(dt)
  if(!missing(index_chunks)){
    setkeyv(dt, c(index_vars[index_chunks], index_vars[-index_chunks]) )
  }else{
    setkeyv(dt, index_vars)
  }

  chnks = getChunkDef(dt, index_vars, n_chunks, len_chunks, index_chunks)


  ## Apply chunkids to each row
  dt[,"chunk_id" := integer(0)]
  pwalk(chnks, function(rstart, rend, nchnk) dt[rstart:rend, chunk_id := nchnk] )

  ## Chunk and save data
  dir.create(file.path(loc,ref_name))
  pwalk(chnks,function(rstart, rend, nchnk) fst::write_fst(dt[rstart:rend],file.path(loc,ref_name,paste0(nchnk,".fst"))))

  ## Get index start end for each row
  for(i in index_vars){
    dt[, by = chunk_id, getLengths(get(i)) ] %>%
      unique %>%
      write_fst(file.path(loc, ref_name, paste0("index_",i,".fst")) )
  }

  meta = addMeta(loc, ref_name, index_vars, chnks, type = "static", silent=F)

  addTblList(ref_name, loc, silent = T)
  if(!silent){
    cat("Table", ref_name,"written to",loc,"\n")
    return(meta)
  }


}

#' Read a Static Data Source
#'
#' This function will return data filtered using the given index
#'
#' @param ref_name character: Name of the table as set in the DB
#' @param loc character: path of the DB. Can be set globally with \link[sailfish]{set.loc}
#' @param index_var character: index variable to use for loading.
#' @param index_val character (vector): If \code{index_var} is of length 1, then this can be a vector of characters
#' @param index_val_vec character (vector): If \code{index_var} if of length > 1, then this needs to be used if you want to condition on multiple
#' values of the index. The different values of the index need to be separated by a '-' (e.g. \code{'UA-EWR'})
#' @param cols character vector: Vector of column names to load from the index
#' @param fn function: function to apply after reading each chunk.
#'
#' @return data.table containing read data
#' @export
#'
#' @import furrr
#' @import data.table
#' @import fst
#'
#'
#' @examples
#' ## Reading from a single index
#' readStaticData(ref_name = "flights"
#'               , loc = "~/saildb"
#'               , index_var = "carrier"
#'               , index_val = c("UA","9E"))
#'
#' ## Reading from multipler indices
#' readStaticData(ref_name = "flights"
#'               , loc = "~/saildb"
#'               , index_var = c("carrier","origin")
#'               , index_val_vec = c("UA-JFK","9E-EWR"))
#'
#' ## Read all data
#' readStaticData(ref_name = "flights"
#'               , loc = "~/saildb")
#'
#' ## Read and apply function
#' readStaticData("flights"
#'               , index_var = "carrier"
#'               , index_val = "UA"
#'               , fn = function(dt) {dt[, path := paste0(origin,"-",dest)]})
readStaticData <- function(ref_name, loc, index_var, index_val, index_val_vec, cols = NULL, fn = identity){



  if(missing(loc)) loc = getVar("loc")
  if(missing(ref_name))
    stop("need fsdt or ref_name")
  if(!(ref_name %in% getTblList(loc)))
    stop(ref_name, " not found in table index")
  if(getMeta(ref_name, loc)$type != "static")
    stop(ref_name, " is not a static table")
  if(((missing(index_var) & !missing(index_val)) | (!missing(index_var) & missing(index_val))) &
     ((missing(index_var) & !missing(index_val_vec)) | (!missing(index_var) & missing(index_val_vec))))
    stop("Both index_var and index_val/index_val_vec must be given")
  if(!missing(index_var) && !missing(index_val) && length(index_var)!=length(index_val))
    stop("length of index_val must equal index_var")

  meta = getMeta(ref_name, loc)

  if(!missing(index_var) && !all(index_var %in% meta$indexed_by))
    stop("not all index_var are in index")


  if(missing(index_var)){

    dt = future_map(1:meta$n_chunks
                    , function(x) read.fst(file.path(loc,ref_name,paste0(x,".fst"))
                                           , columns = cols
                                           , as.data.table = T) %>% fn ) %>%
      rbindlist()

  }else{

    index = fst::read.fst(file.path(loc,ref_name,paste0("index_",index_var[1],".fst")),as.data.table = T)

    if(length(index_var)>1){
      for(i in 2:(length(index_var))){
        y = fst::read.fst(file.path(loc,ref_name,paste0("index_",index_var[i],".fst")),as.data.table = T)
        index = merge(index, y, by = "chunk_id", all = T, allow.cartesian = T) %>%
          .[,intersectIndex(chunk_id, var.x, var.y, rstart.x, rend.x, rstart.y, rend.y)]
      }
    }

    val_cond = if(!missing(index_val)) paste0(index_val,collapse = "-") else index_val_vec

    index = index[var %in% val_cond & !is.na(rstart) ]
    dt = future_pmap(index
                     , function(chunk_id, rstart, rend, var)
                        read.fst(file.path(loc,ref_name,paste0(chunk_id,".fst"))
                                ,from = rstart
                                , to = rend
                                , columns = cols
                                , as.data.table = T) %>% fn ) %>%
      rbindlist()
  }

  return(dt)
}









