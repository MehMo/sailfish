
<!-- README.md is generated from README.Rmd. Please edit that file -->

# sailfish

<!-- badges: start -->
<!-- badges: end -->

The goal of *sailfish* is be a R based DBMS that uses the
[fst](http://www.fstpackage.org/) package in the backend with
[plumber](https://www.rplumber.io/) REST APIs to connect to clients.
This project is in early development and only some early functionalities
are available. The focus at the moment is to enable the reading and
writing of an indexed static data source.

## Installation

You can install the released version of sailfish from
[GitLab](https://gitlab.com/MehMo/sailfish) with:

``` r
remotes::install_gitlab("MehMo/sailfish")
```

## Example

Basic example:

``` r
library(sailfish)

setWorkers(n_workers = 4)
set.loc("~/saildb")

data = nycflights13::flights
ingestStaticData(data
                 , ref_name = "flights"
                 , n_chunks = 10
                 , index_vars = c("carrier", "origin", "dest")
                 , overwrite = T
                 , silent = T)

readStaticData("flights"
               , index_var = c("carrier","origin")
               , index_val_vec = c("UA-JFK", "9E-EWR")
               , cols = "dep_time")
```
